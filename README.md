
# Sistemas Distribuidos - Trabajo Práctico  - Unidad III

## Cliente

_Trabajo práctico de la materia Sistemas Distribuidos._
_Implementación en Java de un cliente para la conexión al [servidor](https://gitlab.com/ssruiz/servidor_sd_tp) multihilo utilizando sockets TCP ._

---
### Requerimientos de instalación 📋

```
JDK instalado.
IDE con soporte Maven(Se utilizó Netbeans).
```
---
### Instalación
```
	1. Descargar o clonar el repositorio
	2. Abir con el IDE de preferencia ( Netbeans/Eclipse/...)
	3. Actualizar y descargar las dependencias maven.
	4. Realizar un Build.
	5. Ejecutar:
            5.1 Desde el IDE: si se usó Netbeans "Run Project" el archivo de entrada ya está configurado. En caso de usar otro IDE la clase de entrada se encuentra en el package view con nombre ClienteForm, ejecutar ese archivo.
		5.2 Desde una consola: situarse en la carpeta tarjet luego de realizar el build y ejecurar el .jar generado: 
		    $ java -jar cliente_sd_tp-1.0-SNAPSHOT.
```
---
## Generar Documentación 📄

#### Para generación de la documentación:
    click derecho en la raiz del proyecto > Generate Javadoc_
    Resultado en la carpeta target

## Versionado 📌

Se utilizó [Git](https://git-scm.com) para el versionado.


## Autores ✒️

* **Samuel Sebastian Ruiz Ibarra**  - [ssruiz](https://gitlab.com/ssruiz)