/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cliente_sd_tp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import javax.swing.JOptionPane;
import models.MensajeJson;
import models.MensajeModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import util.MenuUtil;
import views.ClienteForm;

/**
 * Clase que define un hilo para la lectura de mensajes envíados desde el
 * servidor
 *
 * @author sam
 */
public class ReadThread extends Thread {

    private boolean logged;
    private MensajeModel mm;
    private MenuUtil mu;
    private MensajeJson mj;
    Socket socketConexion = null;
    DataInputStream socketEntrada;
    DataOutputStream socketSalida;
    private boolean enLlamada;
    private String username;
    ClienteForm form;

    /**
     * Constructor de la clase
     * @param s  Socket perteneciente al usuario.
     * @param di Stream de entrada correspondiente al socket del cliente
     * @param dou Stream de salida correspondiente al socket del cliente
     * @param u Nombre del usuario
     */
    public ReadThread(Socket s, DataInputStream di, DataOutputStream dou, String u) {
        try {
            username = u;
            socketConexion = s;
            socketEntrada = di;
            socketSalida = dou;
            enLlamada = false;
            mm = new MensajeModel();
            logged = true;
            mu = new MenuUtil();
            mj = new MensajeJson();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Inicia el hilo
     */
    @Override
    public void run() {
        try {
            JSONParser parser = new JSONParser();
            mu.setForm(getForm());
            while (logged) {
                try {
                    // Se esperan mensajes entrantes
                    String msg = socketEntrada.readUTF();
                    mm = new MensajeModel();
                    mm = mj.stringObjeto(msg);
                    if (mm.getEstado() == 0) {
                        switch (mm.getTipoOperacion()) {
                            // Operacion de de listado
                            case 1:
                                mu.imprimirListadoClientes(mm.getCuerpo());
                                break;
                            // Operación de llamada. 
                            case 2:
                                JSONObject jsonObject = (JSONObject) parser.parse(mm.getCuerpo());
                                String origen = (String) jsonObject.get("origen");
                                String destino = (String) jsonObject.get("destino");
                                System.out.println("USERNAME" + username);
                                System.out.println("ORGIEN" + origen);
                                System.out.println("DESTINO" + destino);
                                
                                // Llamada realizada por este usuario
                                if (origen.equals(username)) {
                                    
                                    //se inicia un hilo para la llamada
                                    LlamadaThread tl = new LlamadaThread(2, username, destino, socketEntrada, socketSalida, getForm());
                                    tl.start();
                                    
                                    // este hilo se suspende hasta que finalice la llamada
                                    tl.join();
                                    break;
                                } 
                                // Llamada entrante
                                else {
                                    //se inicia un hilo para la llamada
                                    LlamadaThread tl = new LlamadaThread(1, username, origen, socketEntrada, socketSalida, getForm());
                                    tl.start();
                                    // este hilo se suspende hasta que finalice la llamada
                                    tl.join();
                                    break;
                                }
                            // operación de desconexión
                            case 4:
                                //json = socketEntrada.readUTF();
                                System.out.println(msg);
                                logged = false;
                                break;
                            // Operación: Mensaje instantaneo
                            case 6:
                                System.out.println(mm.getMensaje());

                                JSONObject jsonObject2 = (JSONObject) parser.parse(mm.getCuerpo());

                                String mensajeCuerpo = (String) jsonObject2.get("mensaje");
                                this.form.getTxtChatLog().append(mm.getMensaje() + "\n");
                                this.form.getTxtChatLog().append("Mensaje--> " + mensajeCuerpo + "\n");

                                break;
                            // Operación: llamada rechazada
                            case 14:
                                Object[] buttons = {"OK"};
                                JSONObject jsonObject3 = (JSONObject) parser.parse(mm.getCuerpo());

                                String mensajeCuerpo3 = (String) jsonObject3.get("destino");
                                int res = JOptionPane.showOptionDialog(
                                        getForm(), 
                                        mensajeCuerpo3 + " se encuentra ocupado/a . Intente más tarde", "Llamada rechazada", 
                                        JOptionPane.PLAIN_MESSAGE, 
                                        JOptionPane.INFORMATION_MESSAGE, null, 
                                        buttons, 
                                        buttons[0]);

                                break;

                        }
                    } else if (mm.getEstado() > 1) {
                        this.form.getTxtChatLog().append("Un error ha ocurrido" + "\n" + "Codigo error: " + mm.getEstado() + "\n" + "Detalle: " + mm.getMensaje() + "\n");
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    logged = false;

                } catch (Exception ex) {
                    ex.printStackTrace();
                    logged = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socketEntrada.close();
                socketEntrada.close();
                socketConexion.close();
            } catch (Exception e) {
            }
        }

    }
    
    /**
     * Getter del formulario del cliente
     * @return Referencia al UI del cliente
     */
    public ClienteForm getForm() {
        return form;
    }

    /**
     * Setter del form del cliente
     * @param form Referencia l formulario(UI) del cliente
     */
    public void setForm(ClienteForm form) {
        this.form = form;
    }

//    /**
//     * Metódo
//     * @param destino 
//     */
//    public void iniciarLlamada(String destino) {
//        try {
//            System.out.println("Hola");
//            mm = new MensajeModel();
//            mm.setEstado(0);
//            mm.setMensaje("ok");
//            mm.setTipoOperacion(2);
//            mm.setCuerpo(getCuerpoMensajeIniciarLlamada("Iniciar llamada", destino));
//
//            socketSalida.writeUTF(mj.objetoString(mm));
//
//            LlamadaThread tl = new LlamadaThread(2, username, destino, socketEntrada, socketSalida, getForm());
//
//            tl.start();
//            tl.join();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public String getCuerpoMensajeIniciarLlamada(String msg, String destino) {
//        JSONObject js = new JSONObject();
//        js.put("origen", username);
//        js.put("mensaje", msg);
//        js.put("destino", destino);
//        return js.toJSONString();
//    }

}
