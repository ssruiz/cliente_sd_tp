/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cliente_sd_tp;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import models.MensajeJson;
import models.MensajeModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import util.MenuUtil;
import views.ClienteForm;
import views.LlamadaForm;

/**
 * Clase que implementa un hilo encargado de recibir los mensajes del otro usuario participante de la llamada.
 * @author Samuel Ruiz
 */
public class LlamadaRead extends Thread {

    private boolean logged;
    private MensajeModel mm;
    private MenuUtil mu;
    private MensajeJson mj;
    DataInputStream socketEntrada;
    DataOutputStream socketSalida;
    private boolean enLlamada;
    private String emisor;
    private LlamadaForm cliente;

    /**
     * Constructor de la clase
     *
     * @param di Stream de entrada correspondiente al socket del cliente
     */
    public LlamadaRead(DataInputStream di, String em, LlamadaForm c) {
        try {
            socketEntrada = di;
            setEmisor(em);
            cliente = c;
            enLlamada = false;

            mm = new MensajeModel();
            logged = true;
            mu = new MenuUtil();
            mj = new MensajeJson();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Inicia el hilo
     */
    @Override
    public void run() {
        try {
            while (logged) {
                try {
                    // read the message sent to this client 
                    String msg = socketEntrada.readUTF();
                    mm = new MensajeModel();
                    mm = mj.stringObjeto(msg);
                    if (mm.getEstado() == 0) {
                        switch (mm.getTipoOperacion()) {
                            case 15:
                                System.out.println(msg);
                                JSONParser parser = new JSONParser();
                                JSONObject jsonObject = (JSONObject) parser.parse(mm.getCuerpo());
                                String mensajeCuerpo = (String) jsonObject.get("mensaje");
                                String em = (String) jsonObject.get("origen");
                                escribirMensajeEnCliente(em+":", mensajeCuerpo);
                                break;
                            case 16:
                                //json = socketEntrada.readUTF();
                                //escribirMensajeEnCliente("Mensaje--> " + msg);
                                escribirMensajeEnCliente(getEmisor(),mm.getMensaje());

                                System.out.println(msg);
                                logged = false;
                                break;

                        }
                    } else if (mm.getEstado() > 1) {
                        System.out.println("Un error ha ocurrido");
                        System.out.println("Codigo error: " + mm.getEstado());
                        System.out.println("Detalle: " + mm.getMensaje());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    /**
     * Método que se encarga de imprimir el mensaje recibido en la ventana del usuario
     * @param s El usuario emisor
     * @param msg El mensaje enviado
     */
    private void escribirMensajeEnCliente(String s, String msg) {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.ORANGE);
        //aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        //aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = getCliente().getTxtAreaMensajes().getDocument().getLength();
        getCliente().getTxtAreaMensajes().setCaretPosition(len);
        getCliente().getTxtAreaMensajes().setCharacterAttributes(aset, false);
        getCliente().getTxtAreaMensajes().replaceSelection(s);

        AttributeSet aset2 = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.BLACK);
      
        len = getCliente().getTxtAreaMensajes().getDocument().getLength();
        getCliente().getTxtAreaMensajes().setCaretPosition(len);
        getCliente().getTxtAreaMensajes().setCharacterAttributes(aset2, false);
        getCliente().getTxtAreaMensajes().replaceSelection(msg+"\n");

    }
    
    /**
     * Getter del usuario emisor
     * @return Cadena que representa al emisor
     */
    public String getEmisor() {
        return emisor;
    }
    
    /**
     * Setter del usuario emisor
     * @param emisor El emisor de la llamada
     */
    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }
    
    /**
     * Getter del form de la llamada
     * @return El formulario o ventana donde se realiza la llamada
     */
    public LlamadaForm getCliente() {
        return cliente;
    }

    /**
     * Setter del form
     * @param cliente Form donde se realizará la llamada
     */
    public void setCliente(LlamadaForm cliente) {
        this.cliente = cliente;
    }
}
