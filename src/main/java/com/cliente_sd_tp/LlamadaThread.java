/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cliente_sd_tp;

import controller.ConnectionService;
import controller.TipoMensajes;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;

import javax.swing.JOptionPane;
import models.MensajeJson;
import models.MensajeModel;
import util.MenuUtil;
import views.ClienteForm;
import views.LlamadaForm;

/**
 * Clase encargada de crear un hilo para la llamada entre dos usuarios
 *
 * @author Samuel Ruiz
 */
public class LlamadaThread extends Thread {

    private int tipo;
    private MenuUtil mu;
    private String emisor;
    private String receptor;

    private boolean continuar;
    DataInputStream socketEntrada;
    DataOutputStream socketSalida;
    ConnectionService cs;
    MensajeModel mm;
    MensajeJson mj;
    ClienteForm cliente;
    LlamadaForm llamada;
    private Thread predecessor;

    public Thread getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(Thread predecessor) {
        this.predecessor = predecessor;
    }

    /**
     *
     * @param tipo Define el tipo de usuario al iniciar la llamada. 1= Es un
     * Usuario a quien se llama. 2 = es un usuario que inicio la llamada
     * @param emisor Nombre del usuario que será el emisor en este hilo.
     * @param receptor Nombre del usuario que será el receptor en este hilo.
     * @param se Stream de lectura del socket emisor, necesario para recibir la
     * conversación
     * @param ss Stream de salida del socket. Usado para mandar mensajes al
     * receptor.
     * @param cliente Referencia al formulario(UI) del cliente
     */
    public LlamadaThread(int tipo, String emisor, String receptor, DataInputStream se, DataOutputStream ss, ClienteForm cliente) {
        setTipo(tipo);
        setCliente(cliente);
        setEmisor(emisor);
        setReceptor(receptor);
        setSocketEntrada(se);
        setSocketSalida(ss);
        setContinuar(true);
        mu = new MenuUtil();
        cs = new ConnectionService();
        mm = new MensajeModel();
        mj = new MensajeJson();
    }

    /**
     * Inicializa el hilo
     */
    @Override
    public void run() {
        try {
            // mu.limpiarPantalla();
            // Cliente receptor. El qure recibe la llamada
            if (tipo == 1) {
                //mu.imprimirLlamadaEntrante(getReceptor());
                //  int res = cs.aceptarLlamada(getReceptor());
                Object[] options = {"Aceptar", "Rechazar"};
                String aux = "Llamada entrante de: " + getReceptor();
                int a = JOptionPane.showOptionDialog(
                        getCliente(),
                        aux, "Llamada entrante",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, null,
                        options,
                        options[0]);

                //int a = JOptionPane.showConfirmDialog(getCliente(), aux, "Llamada", JOptionPane.YES_NO_OPTION);
                if (a == JOptionPane.YES_OPTION) {
                    socketSalida.writeUTF(cs.getMensajeHablar(getEmisor(), getReceptor(), "Llamada aceptada", 0, 15));
                    //loop
                    setContinuar(true);
                    // mu.limpiarPantalla();
                    //System.out.println("Conexión aceptada. Puede hablar con: " + getReceptor() + "(Inrgrese 0 para finalizar la llamada)");
                    //escribirMensajeEnCliente("Conexión aceptada. Puede hablar con: " + getReceptor());

                    llamada = new LlamadaForm(getCliente(), true);
                    llamada.setModalityType(Dialog.ModalityType.MODELESS);
                    inicioLlamada();
                    llamadaAceptada(getReceptor());
                    llamada.setLocationRelativeTo(cliente);
                    llamada.setVisible(true);
                    LlamadaRead readThreadLlamada = new LlamadaRead(socketEntrada, getReceptor(), getLlamada());
                    readThreadLlamada.start();
                    while (readThreadLlamada.isAlive()) {

                    }

                    getLlamada().setVisible(false);
                    getLlamada().dispose();
                } // llamada aceptada
                //                if (res == TipoMensajes.RECEPTOR_ACCEPT.getLevelCode()) {
                //
                //                } //llamada rechazada
                else {
                    socketSalida.writeUTF(cs.getMensajeHablar(getEmisor(), getReceptor(), "Llamada rechazada", 0, TipoMensajes.CANCEL_CALL.getLevelCode()));
                    System.out.println("Llamada rechazada");

                }
                // cliente que inicio la llamada
            } else {
                String mensajeEntrante;
                String mensajeSaliente;
                escribirMensajeEnCliente("Esperando a : " + getReceptor());
                llamada = new LlamadaForm(getCliente(), true);
                llamada.setModalityType(Dialog.ModalityType.MODELESS);
                inicioLlamada();
                llamada.setLocationRelativeTo(cliente);
                llamada.setVisible(true);

                System.out.println("Esperando a ");
                mensajeEntrante = socketEntrada.readUTF();
                System.out.println("Contesta a ");

                mm = mj.stringObjeto(mensajeEntrante);

                // llamada aceptada desde el receptor
                if (mm.getEstado() == 0) {
//                    System.out.println("Aceptada ");

                    escribirMensajeEnCliente("Conexión aceptada. Puede hablar con: " + getReceptor());
                    llamadaAceptada(getReceptor());
                    LlamadaRead readThreadLlamada = new LlamadaRead(socketEntrada, getReceptor(), getLlamada());

                    if (mm.getTipoOperacion() != 14) {
                        setContinuar(true);
                        readThreadLlamada.start();
                        while (readThreadLlamada.isAlive()) {

                        }

                        getLlamada().setVisible(false);
                        getLlamada().dispose();

                    } else {
                        Object[] buttons = {"OK"};
                        int res = JOptionPane.showOptionDialog(
                                getLlamada(),
                                getReceptor() + " se encuentra ocupado/a . Intente más tarde",
                                "Llamada rechazada",
                                JOptionPane.PLAIN_MESSAGE,
                                JOptionPane.INFORMATION_MESSAGE, null,
                                buttons,
                                buttons[0]);
                        getLlamada().setVisible(false);
                        getLlamada().dispose();
                        setContinuar(false);
                        System.out.println("llamada rechazada desde el receptor");
                    }
                } // llamada rechazada
                else {
                    JOptionPane.showConfirmDialog(getLlamada(), getReceptor() + " se encuentra ocupado/a . Intente más tarde", "Llamada", JOptionPane.OK_OPTION);
                    getLlamada().setVisible(false);
                    getLlamada().dispose();
                    System.out.println("Un error ha ocurrido");
                    System.out.println("Codigo error: " + mm.getEstado());
                    System.out.println("Detalle: " + mm.getMensaje());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que setea una actionListener en el boton de envio. Permitiendo el
     * envio de un mensaje escrito
     */
    private ActionListener listenEnvios = new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {

            try {
                String msg = llamada.getTxtMandarMensaje().getText();
                mm = new MensajeModel();
                mm.setEstado(0);
                mm.setMensaje("ok");
                mm.setTipoOperacion(15);
                mm.setCuerpo(cs.getCuerpoMensajeCToC(msg, getEmisor(), getReceptor()));
                String r = mj.objetoString(mm);
                socketSalida.writeUTF(r);
            } catch (Exception e) {
            }

        }
    };

    /**
     * Metodo que setea una actionListener en el boton de finalizar la llamada.
     * Permitiendo cortar la misma.
     */
    private ActionListener terminarLlamada = new ActionListener() {
        public void actionPerformed(ActionEvent arg01) {

            Object[] options = {"Si", "No"};
            String aux = "Terminar la llamada con: " + getReceptor();
            int a = JOptionPane.showOptionDialog(
                    getCliente(),
                    aux, "Llamada entrante",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.INFORMATION_MESSAGE, null,
                    options,
                    options[0]);
            try {
                if (a == JOptionPane.YES_OPTION) {
                    String msg = llamada.getTxtMandarMensaje().getText();
                    mm = new MensajeModel();
                    mm.setEstado(-1);
                    mm.setMensaje("ok");
                    mm.setTipoOperacion(16);
                    mm.setCuerpo(cs.getCuerpoMensajeCToC(msg, getEmisor(), getReceptor()));
                    String r = mj.objetoString(mm);
                    socketSalida.writeUTF(r);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    /**
     * Escribe un mensaje en el UI del usuario.
     *
     * @param s Mensaje a mostrar
     */
    private void escribirMensajeEnCliente(String s) {
        getCliente().getTxtChatLog().append(s + "\n");
    }

    /**
     * Establece los parámetros y configuración por defecto del formulario del
     * usuario
     */
    private void inicioLlamada() {
        llamada.getBtnMandarMensaje().setEnabled(false);
        llamada.getBtnTerminarLlamada().setEnabled(false);
        llamada.getLblStatus().setText("Esperando a : " + getReceptor());
        llamada.getLblTitulo().setText("Llamada a:" + getReceptor());
        // llamada.getTxtAreaMensajes().setEnabled(false);
        llamada.getTxtMandarMensaje().setEnabled(false);
        //llamada.getBtnMandarMensaje().addActionListener(listenEnvios);
    }

    /**
     * Establece los parámetros y configuración de los elementos gráficos en
     * caso de que la llamada fue aceptada
     *
     * @param conectado
     */
    private void llamadaAceptada(String conectado) {
        llamada.getBtnMandarMensaje().setEnabled(true);
        llamada.getBtnTerminarLlamada().setEnabled(true);
        llamada.getLblStatus().setText("Conectado a : " + conectado);
        llamada.getLblTitulo().setText("Llamada en curso con: " + conectado);
        //llamada.getTxtAreaMensajes().setEnabled(false);
        llamada.getTxtMandarMensaje().setEnabled(true);
        llamada.getBtnMandarMensaje().addActionListener(listenEnvios);
        llamada.getBtnTerminarLlamada().addActionListener(terminarLlamada);

    }

    /**
     * Getter del tipo
     *
     * @return El tipo de usuario al inicio de la llamda.
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * Setter del tipo
     *
     * @param tipo El tipo de usuario al inicio de la llamda.
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * Getter el stream de entrada.
     *
     * @return El stream de entrada
     */
    public DataInputStream getSocketEntrada() {
        return socketEntrada;
    }

    /**
     * Setter del stream de entrada del socket
     *
     * @param socketEntrada El stream de entrada del socket
     */
    public void setSocketEntrada(DataInputStream socketEntrada) {
        this.socketEntrada = socketEntrada;
    }

    /**
     * Getter del stream de salida del socket.
     *
     * @return Stream de salida del socket.
     */
    public DataOutputStream getSocketSalida() {
        return socketSalida;
    }

    /**
     * Setter del stream de salida del socket
     *
     * @param socketSalida Stream de salida del socket.
     */
    public void setSocketSalida(DataOutputStream socketSalida) {
        this.socketSalida = socketSalida;
    }

    /**
     * Getter del usuario del hilo
     *
     * @return El nombre del usuario.
     */
    public String getEmisor() {
        return emisor;
    }

    /**
     * setter del nombre del usuario.
     *
     * @param emisor Nombre del usuario
     */
    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    /**
     * Getter de continuar
     *
     * @return Si el ciclo infinito debe continuar
     */
    public boolean isContinuar() {
        return continuar;
    }

    /**
     * setter de continuar
     *
     * @param continuar valor de si el ciclo infinito debe continuar
     */
    public void setContinuar(boolean continuar) {
        this.continuar = continuar;
    }

    /**
     * Getter del receptor
     *
     * @return Nombre del otro usuario en la otra llamada
     */
    public String getReceptor() {
        return receptor;
    }

    /**
     * Setter del receptor
     *
     * @param receptor Nombre del otro usuario en la otra llamada
     */
    public void setReceptor(String receptor) {
        this.receptor = receptor;
    }

    /**
     * Getter del form del usuario
     *
     * @return Referencia al UI del usuario
     */
    public ClienteForm getCliente() {
        return cliente;
    }

    /**
     * Setter del form del cliente
     *
     * @param cliente Referencia al UI del usuario
     */
    public void setCliente(ClienteForm cliente) {
        this.cliente = cliente;
    }

    /**
     * Getter del form de llamada.
     *
     * @return Dialogo donde se realizará la llamada
     */
    public LlamadaForm getLlamada() {
        return llamada;
    }

    /**
     * Setter del form de la llamada.
     *
     * @param llamada Dialogo donde se realizará la llamada
     */
    public void setLlamada(LlamadaForm llamada) {
        this.llamada = llamada;
    }
}
