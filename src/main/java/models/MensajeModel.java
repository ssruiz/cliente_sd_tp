/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 * Clase que especifica el modelo de mensajes enviados entre los usuarios y el servidor
 * @author sam
 */
public class MensajeModel {

    private int estado;
    private int tipoOperacion;
    private String mensaje;
    private String cuerpo;

    /**
     * Getter del estado
     * @return Estado del mensaje
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Setter del estado del mensaje
     * @param estado 
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * Getter del mensaje
     * @return El mensaje del estado
     */
    public String getMensaje() {
        return mensaje;
    }
    
    /**
     * Setter del mensaje del estado
     * @param mensaje Mensaje del estado
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * Getter del tipo de operación
     * @return Tipo de operación
     */
    public int getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * Setter del tipo de operación
     * @param tipoOperacion Operación a asignar
     */
    public void setTipoOperacion(int tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }
    
    /**
     * Getter del cuerpo del mensaje
     * @return Cuerpo del mensaje
     */
    public String getCuerpo() {
        return cuerpo;
    }
    
    /**
     * Setter del Cuerpo del mensaje
     * @param cuerpo Cuerpo del mensaje
     */
    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
    
    /**
     * Método que devuelve una representación de la clase
     * @return 
     */
    @Override
    public String toString() {
        return "MensajeModel [estado=" + estado + ", tipoOperacion=" + tipoOperacion + ", mensaje=" + mensaje
                + ", cuerpo=" + cuerpo.toString() + "]";
    }
}
