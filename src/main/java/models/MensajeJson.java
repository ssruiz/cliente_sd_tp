/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Clase que maneja los mensajes en formato Json para enviar y recibidos
 *
 * @author sam
 */
public class MensajeJson {

    /**
     * Método que transforma un objeto a json
     *
     * @param m El objeto a transformar
     * @return El objeto en json
     */
    public String objetoString(MensajeModel m) {

        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        obj.put("estado", m.getEstado());
        obj.put("mensaje", m.getMensaje());
        obj.put("tipoOperacion", m.getTipoOperacion());
        String cuerpo = m.getCuerpo();
        obj.put("cuerpo", cuerpo);
        return obj.toJSONString();
    }

    /**
     * Método que transforma un json a un objeto
     * @param str El json a convertir
     * @return El objeto MensajeModel 
     */
    public MensajeModel stringObjeto(String str) {
        try {
            MensajeModel p = new MensajeModel();
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(str.trim());
            JSONObject jsonObject = (JSONObject) obj;
            Long aux = (Long) jsonObject.get("estado");
            p.setEstado(aux.intValue());
            p.setMensaje((String) jsonObject.get("mensaje"));
            aux = (Long) jsonObject.get("tipoOperacion");
            p.setTipoOperacion(aux.intValue());
            String msg = (String) jsonObject.get("cuerpo");
            p.setCuerpo(msg);
            return p;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
