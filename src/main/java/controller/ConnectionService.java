/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.cliente_sd_tp.LlamadaThread;
import com.cliente_sd_tp.ReadThread;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.CountDownLatch;
import models.MensajeJson;
import models.MensajeModel;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import util.MenuUtil;
import views.ClienteForm;

/**
 * Clase que se encarga de realizar las peticiones al servidor
 *
 * @author Samuel Ruiz
 */
public class ConnectionService {

    private static Logger logger = Logger.getLogger(ConnectionService.class);
    Socket socketConexion = null;
    DataInputStream socketEntrada;
    DataOutputStream socketSalida;
    MensajeModel mm;
    MensajeJson mj;
    MenuUtil mu;
    ReadThread readThread;
    private String username;
    private BufferedReader br;
    CountDownLatch gate;
    ClienteForm form;

    /**
     * Constructor de la clase
     */
    public ConnectionService() {
        try {
            //PropertyConfigurator.configure("log4j.properties");
            BasicConfigurator.configure();
            username = "";
            //socketConexion = socket;
            //socketSalida = new DataOutputStream(socketConexion.getOutputStream());
            mm = new MensajeModel();
            mj = new MensajeJson();
            mu = new MenuUtil();
            br = new BufferedReader(new InputStreamReader(System.in));

        } catch (Exception e) {
        }
    }



    /**
     * Inicia el hilo que se encargará de recibir las respuestas del servidor
     */
    public void iniciarHiloLectura() {
        try {
            //LlamadaThread tl = new LlamadaThread(1, username, "", socketEntrada, socketSalida);
            //tl.join();
            readThread = new ReadThread(socketConexion, socketEntrada, socketSalida, getUsername());
            readThread.setForm(this.getForm());
            readThread.start();
        } catch (Exception e) {
        }

    }

    /**
     * Pide el listado de usuarios conectados
     */
    public void pedirListado() {
        try {
            String mensaje = prepararMensaje(1);
            socketSalida.writeUTF(mensaje);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Inicia una llamada entre dos usuarios
     */
    public void iniciarLlamada() {

        try {
            System.out.println("Ingrese el usuario a quien desea enviar");
            String destino = br.readLine();
            mm = new MensajeModel();
            mm.setEstado(0);
            mm.setMensaje("ok");
            mm.setTipoOperacion(2);
            mm.setCuerpo(getCuerpoMensajeIniciarLlamada("Iniciar llamada", destino));

            socketSalida.writeUTF(mj.objetoString(mm));
            LlamadaThread tl = new LlamadaThread(2, getUsername(), destino, socketEntrada, socketSalida, null);
            tl.start();
            tl.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void iniciarLlamada2(String destino) {

        try {

            mm = new MensajeModel();
            mm.setEstado(0);
            mm.setMensaje("ok");
            mm.setTipoOperacion(2);
            mm.setCuerpo(getCuerpoMensajeIniciarLlamada("Iniciar llamada", destino));

            socketSalida.writeUTF(mj.objetoString(mm));

            // LlamadaThread tl = new LlamadaThread(2, getUsername(), destino, socketEntrada, socketSalida, getForm());
            // this.readThread.iniciarLlamada(destino);
            //  tl.start();
            // tl.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Envía un mensaje al usuario ingresado por consola
     */
    public void enviarMensaje() {
        try {

            //System.out.println("Ingrese el mensaje a enviar");
            //String msg = br.readLine();
            //System.out.println("Ingrese el usuario a quien desea enviar");
            //String destino = br.readLine();
            mm = new MensajeModel();
            mm.setEstado(-1);
            mm.setMensaje("ok");
            mm.setTipoOperacion(3);
            mm.setCuerpo(getCuerpoMensajeCToC(getForm().getTxtMensajes().getText(), getUsername(), getForm().getTxtDestino().getText()));
            socketSalida.writeUTF(mj.objetoString(mm));
        } catch (Exception e) {

        }

    }

    public void hablar() {
        try {

            //System.out.println("Ingrese el mensaje a enviar");
            //String msg = br.readLine();
            //System.out.println("Ingrese el usuario a quien desea enviar");
            //String destino = br.readLine();
            mm = new MensajeModel();
            mm.setEstado(-1);
            mm.setMensaje("ok");
            mm.setTipoOperacion(3);
            mm.setCuerpo(getCuerpoMensajeCToC(getForm().getTxtMensajes().getText(), getUsername(), getForm().getTxtDestino().getText()));
            socketSalida.writeUTF(mj.objetoString(mm));
        } catch (Exception e) {

        }

    }

    /**
     * Permite la desconexión del usuario al servidor
     */
    public void desconexion() {
        try {
            String mensaje = prepararMensaje(4);
            socketSalida.writeUTF(mensaje);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Prepara el mensaje en formato Json para comunicarse con el servidor
     *
     * @param op Contiene el tipo de operación que se realizará
     * @return Retorna el mensaje en formato Json de acuerdo al tipo de
     * operación solicitada
     */
    public String prepararMensaje(int op) {

        switch (op) {
            case 0:
                mm = new MensajeModel();
                mm.setEstado(-1);
                mm.setMensaje("ok");
                mm.setTipoOperacion(0);
                mm.setCuerpo(username);
                return mj.objetoString(mm);
            case 1:
                mm = new MensajeModel();
                mm.setEstado(-1);
                mm.setMensaje("ok");
                mm.setTipoOperacion(1);
                mm.setCuerpo("Get listado");
                return mj.objetoString(mm);
            case 2:
                mm = new MensajeModel();
                mm.setEstado(-1);
                mm.setMensaje("ok");
                mm.setTipoOperacion(2);
                mm.setCuerpo("Iniciar llamada");
                return mj.objetoString(mm);
            case 3:
                mm = new MensajeModel();
                mm.setEstado(-1);
                mm.setMensaje("ok");
                mm.setTipoOperacion(3);

                break;
            case 4:
                mm = new MensajeModel();
                mm.setEstado(-1);
                mm.setMensaje("ok");
                mm.setTipoOperacion(4);
                mm.setCuerpo(username);
                return mj.objetoString(mm);

        }
        return "";
    }

    

    /**
     * Función que permite obtener el cuerpo del mensaje para la operación "3.
     * Enviar mensaje"
     *
     * @param msg El mensaje a transformar
     * @param destino El usuario a quien se lo envía
     * @return Retorna el cuerpo del mensaje en formateo JSON
     */
    public String getCuerpoMensajeCToC(String msg, String origen, String destino) {
        JSONObject js = new JSONObject();
        js.put("origen", origen);
        js.put("mensaje", msg);
        js.put("destino", destino);
        return js.toJSONString();
    }

    public String getCuerpoMensajeIniciarLlamada(String msg, String destino) {
        JSONObject js = new JSONObject();
        js.put("origen", getUsername());
        js.put("mensaje", msg);
        js.put("destino", destino);
        return js.toJSONString();
    }

    

   

    /**
     * Valida que el entero leído en leerOperacion
     * {@link #leerOperacion() leerOperacion}
     *
     * @param value El string a validar
     * @return True si es un entero, false en caso contrario.
     */
    private boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /* --------------------------- Operaciones de llamadas */
    public String getMensajeHablar(String emisor, String receptor, String mensaje, int codigo, int op) {
        mm = new MensajeModel();
        mm.setEstado(codigo);
        mm.setTipoOperacion(op);
        mm.setMensaje(mensaje);
        mm.setCuerpo(getCuerpoMensajeCToC(mensaje, emisor, receptor));
        return mj.objetoString(mm);
    }

    /**
     * Getter del stream de entrada del socket
     *
     * @return Stream de entrada del socket
     */
    public DataInputStream getSocketEntrada() {
        return socketEntrada;
    }

    /**
     * Setter del stream de entrada del socket
     *
     * @param socketEntrada Stream de entrada del socket
     */
    public void setSocketEntrada(DataInputStream socketEntrada) {
        this.socketEntrada = socketEntrada;
    }

    /**
     * Getter del stream de salida del socket
     *
     * @return Stream de entrada del socket
     */
    public DataOutputStream getSocketSalida() {
        return socketSalida;
    }

    /**
     * Setter del stream de salida del socket
     *
     * @param socketEntrada Stream de salida del socket
     */
    public void setSocketSalida(DataOutputStream socketSalida) {
        this.socketSalida = socketSalida;
    }

    /**
     * Getter del form del cliente
     *
     * @return Referencia al formulario del cliente
     */
    public ClienteForm getForm() {
        return form;
    }

    /**
     * Setter del form del cliente
     *
     * @param form Referencia al formulario del cliente
     */
    public void setForm(ClienteForm form) {
        this.form = form;
    }

    /**
     * Getter del atributo username
     *
     * @return Retorna el username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter del atributo usernmae
     *
     * @param username Recibe el nombre del usuario que será asignado a username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
