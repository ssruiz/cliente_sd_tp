/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *  Clase que define los tipos de mensajes
 * @author sam
 */
public enum TipoMensajes {
    MESSAGE(10),
    RECEPTOR_ACCEPT(1),
    RECEPTOR_REFUSE(2),
    CANCEL_CALL(14),
    TALK(15),
    
    END_CALL(16);
    private final int levelCode;

    private TipoMensajes(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return this.levelCode;
    }

}
