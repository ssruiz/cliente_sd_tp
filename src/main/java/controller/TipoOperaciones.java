/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *  Clase que define los tipos de operaciones del sistema
 * @author sam
 */
public enum TipoOperaciones {
    CONNECT(0),
    LIST_CLIENTS(1),
    START_CALL(2),
    SEND_MESSAGE(3),
    DISCONNECT(4)
    ;

    public final int levelCode;

    private TipoOperaciones(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return this.levelCode;
    }

}
