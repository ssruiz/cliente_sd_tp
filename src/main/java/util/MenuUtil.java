/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.Iterator;
import models.MensajeModel;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import views.ClienteForm;

/**
 * Clase que se encarga de impresiones en pantalla
 *
 * @author Samuel Ruiz
 */
public class MenuUtil {
    
    private  ClienteForm form;
    
    /**
     * Getter del formulario del cliente
     * @return Referencia al UI del cliente
     */
    public ClienteForm getForm() {
        return form;
    }
    
    /**
     * Setter del formulario de cliente
     * @param form Referencia al UI del cliente
     */
    public void setForm(ClienteForm form) {
        this.form = form;
    }

    /**
     * Imprime el listado de clientes conectados.
     *
     * @param json. Listado de clientes en formato
     */
    public void imprimirListadoClientes(String mm) {
        try {
            JSONParser parser = new JSONParser();
            JSONArray listado = (JSONArray) parser.parse(mm);
            ArrayList<String> list = new ArrayList<String>();
               this.form.getTxtChatLog().append("---------- Listado de Usuarios Conectados -----------------\n");
            if (listado != null) {
                Iterator<String> iterator = listado.iterator();
                while (iterator.hasNext()) {
                    this.form.getTxtChatLog().append("--" + iterator.next() + "\n");
                    //System.out.println(iterator.next());
                }
            }

        } catch (Exception e) {
        }

    }
}
